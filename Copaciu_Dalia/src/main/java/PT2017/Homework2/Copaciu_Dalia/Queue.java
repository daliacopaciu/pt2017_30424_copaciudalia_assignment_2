package PT2017.Homework2.Copaciu_Dalia;

import java.util.*;
public class Queue {
   private ArrayList<Client> clients;
   private int nr_clients;  //total nr of clients
   private int served;      //nr of served clients
   private int wait;       //will decrease as the clients leave the queue
   private int avg_wait;   //will never decrease, it keeps track of the total waiting time
   private int peak[];    //used to calculate the peak hour
   private Time last_arrival;  //keeps a copy of the last client arrival to compute the waiting time for the next client
   
   public Queue(){
	   clients=new ArrayList<Client>();
	   served=0;
	   wait=0;
	   nr_clients=0;
	   avg_wait=0;
	   last_arrival=new Time();
	   peak=new int[25];
	   for(int i=0;i<24;i++)   //from 8 to 12 => max 5 hours are the candidates for the peak hour
		   peak[i]=0;        //initialize peak hour array
   }
   public void served(){
	   served++;
   }
   public void wait(int n){
	   wait+=n;
	   avg_wait+=n;
   }
   public int get_wait(){
	   return wait;
   }
   public Time last(){
	     return last_arrival;
   }
   public int get_nrclient(char c){
	   if(c=='t')
		   return nr_clients;   //total nr of clients for that queue during the simulation
	   else if (c=='p')
	     return clients.size();   //partial nr of clients= clients at the moment of calling
	   else return served;        //served nr of clients
   }
   public int average(){
     if(avg_wait!=0)
	     return avg_wait/nr_clients;
	 return 0;
   }
   public int peak(){
	   int max=0,maxi=0;
	   for(int i=0;i<24;i++){
		   if(peak[i]>max){
			   max=peak[i];
		       maxi=i;
		   }
	   } 
	   return maxi;
   }
   
   public void add_client(Client c){
	   clients.add(c);
	   last_arrival=c.get_time('a');
	   wait+=c.get_time('s').get_min();
	   avg_wait+=c.get_time('s').get_min();
	   peak[c.get_time('a').get_hour()]++;
	   nr_clients++;
   }
   public void remove_client(){
	   ArrayList<Client>dlt=new ArrayList<Client>();
	   boolean first=true,second=true;
	   Iterator<Client> it=clients.iterator();
	  while(it.hasNext()){
		   Client c=it.next();
		   if(c!=null)
			   if (first){     //delete the 1st client from the queue
			   Time w=c.get_time('w');
			   int k=w.mins(w);
			   wait-=k;                  //the waiting time of the 1st client is substracted from the total waiting time of the queue
			   c.set_Status(Status.done);
			   first=false;
			   dlt.add(c);
		       }
			   else{       
				   Time aux=new Time();
				   c.wait(aux.calc_wait(c.get_time('a'), last(),get_wait()));  //sets waiting time
			   
				   if(second){    //if it's the 2nd client, it is made the 1st client, being served
					   c.set_Status(Status.being_served);
					   served++;
					   second=false;
				   }
			   }
	   }
		   clients.removeAll(dlt);
   }
  public String[] show(){
	  String[] rez=new String[200];
	  int i=0;
	  for(Client c:clients){
		  rez[i]=c.show();
		  i++;
	  }
	  return rez;
  }
  public ArrayList<Client> get_q(){
	   return clients;
  }
  
}
