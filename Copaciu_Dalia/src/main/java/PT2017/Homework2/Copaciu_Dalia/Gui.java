package PT2017.Homework2.Copaciu_Dalia;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;

import java.awt.event.ActionEvent;

import javax.swing.JLabel;
//import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;

public class Gui{
	
	private JFrame frame;
	private JTextArea ta;
	private JTextArea ta2;
	private JTextArea taa3;
	private JTextArea info;
	private JTextArea info2;
	private JTextArea info3;
	private JTextArea ser1;
	private JTextArea ser2;
	private JTextArea ser3;
	private JTextArea p1;
	private JTextArea p2;
	private JTextArea p3;
	private JTextArea pr;
	private JTextArea totp;
	
	private Shop s;
	private JTextField tf;
	private JTextField m1;
	private JTextField m2;
	private JTextField m3;
	private JTextField m4;
	/**
	 * Launch the application.
	 */
	private void clk(int n){
		final int nn=n;
		s.client_come(18);    // add a random nr of clients into the queues for a better displaying
		Thread t1=new Thread(){
			public void run(){
				try{
					Queue q1=s.queue().get(0);    //1st queue
					for(int sec=0;sec<nn/20;sec++){
						ta.setText("");
						String [] y=q1.show();
						if(y.length==0)
							ta.setText("Empty Queue");
						else{
					    for(int i=0;i<q1.get_nrclient('p');i++){   //show the queue at the beginning or after a client was added
						   Thread.sleep(2000);
						   ta.append(y[i]+"\n");
					    }
					    ta.setText("");
					    String upd[]=s.update(q1);    //after removing a client
					    for(int i=0;i<q1.get_nrclient('p');i++){
							  Thread.sleep(2000);
							  if(upd[i]!=null)
							  ta.append(upd[i]+"\n");
						} 
					    s.add_client(q1);   //add a client if it is the min queue, otherwise, no client is added
						}
					  }
					ta.setText("Closed queue");
					info.setText(""+q1.average());        //average waiting time
					ser1.setText(""+q1.get_nrclient('s')); //nr of served clients
					p1.setText(""+s.queue().get(0).peak());  //peak hour
					
					 	
				}	
				 catch(InterruptedException e){
					e.printStackTrace();
				}
			}
		};
		t1.start();
		
		
		Thread t2=new Thread(){
			public void run(){
				try{ 
					Queue q2=s.queue().get(1);
					for(int sec=0;sec<nn/20;sec++){
						ta2.setText("");
						String [] y=q2.show();
						if(y.length==0)
							ta2.setText("Empty Queue");
						else{
						    for(int i=0;i<q2.get_nrclient('p');i++){
							   Thread.sleep(2000);
							   if(y[i]!=null)
							   ta2.append(y[i]+"\n");
						    }
						    ta2.setText("");
						    String upd[]=s.update(q2);
						    for(int i=0;i<q2.get_nrclient('p');i++){
								  Thread.sleep(2000);
								  if(upd[i]!=null)
								  ta2.append(upd[i]+"\n");
							}
						    s.add_client(q2);
						}
						
					}
					ta2.setText("Closed queue");
					info2.setText(""+q2.average());
				    ser2.setText(""+q2.get_nrclient('t'));
				    p2.setText(""+q2.peak());
				}	
				 catch(InterruptedException e){
					e.printStackTrace();
				}
			}
		};
		t2.start();
		
		Thread t3=new Thread(){
			public void run(){
				try{  
					   Queue q3=s.queue().get(2);
					   for(int sec=0;sec<nn/20;sec++){
						   taa3.setText("");
							String [] y=q3.show();
							if(y.length==0)
								taa3.setText("Empty Queue");
							else{
							    for(int i=0;i<q3.get_nrclient('p');i++){
							    	
								   Thread.sleep(2000);
								   if(y[i]!=null)
								   taa3.append(y[i]+"\n");
							    }
							    taa3.setText("");
							    String upd[]=s.update(q3);
							    for(int i=0;i<q3.get_nrclient('p');i++){
									  Thread.sleep(2000);
									  if(upd[i]!=null)
									  taa3.append(upd[i]+"\n");
								} 
							    s.add_client(q3);
							}
							
					   }
					   taa3.setText("Closed queue");
					   info3.setText(""+q3.average());
					   ser3.setText(""+q3.get_nrclient('s'));
					   p3.setText(""+q3.peak());
					   pr.setText(""+s.get_cost());
					   totp.setText(s.peak()+"");
					
				}	
				 catch(InterruptedException e){
					e.printStackTrace();
				}
			}
		};
		t3.start(); 
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui window = new Gui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Gui() {

		initialize();
		//s=new Shop();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	
	private void show_msg(String msg, String title)
    {
        JOptionPane.showMessageDialog(null, msg,title, JOptionPane.INFORMATION_MESSAGE);
    }
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1300, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		ta = new JTextArea();
		ta.setLineWrap(true);
		ta.setBackground(Color.LIGHT_GRAY);
		ta.setEditable(false);
		ta.setBounds(10, 240, 421, 254);
		frame.getContentPane().add(ta);
		ta.setText("");
		
		ta2 = new JTextArea();
		ta2.setLineWrap(true);
		ta2.setBackground(Color.LIGHT_GRAY);
		ta2.setEditable(false);
		ta2.setBounds(451, 240, 400, 254);
		frame.getContentPane().add(ta2);
		ta2.setText("");
		
		JButton btnStart_1 = new JButton("Start");
		btnStart_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnStart_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ta.setText("");
				ta2.setText("");
				taa3.setText("");
				info.setText("");
				info2.setText("");
				info3.setText("");
				ser1.setText("");
				ser2.setText("");
				ser3.setText("");
				p1.setText("");
				p2.setText("");
				p3.setText("");
				pr.setText("");
				try{
				int k=Integer.parseInt(tf.getText().trim());  //nr of secs for simulation
				int min_a=Integer.parseInt(m1.getText().trim());  //max arriving time
				int max_a=Integer.parseInt(m2.getText().trim());  //nr of secs for simulation
				int min_s=Integer.parseInt(m3.getText().trim());  //nr of secs for simulation
				int max_s=Integer.parseInt(m4.getText().trim());  //nr of secs for simulation
				s=new Shop(min_a,max_a,min_s,max_s);
				if(k<0) 
					show_msg("Give the a positive number of seconds for the simulation!","Wrong input");
				else if(k<20)
					show_msg("Give a number greater or equal to 20 so the simulation can start!","Too few minutes");
				else if (min_a<0|| max_a<0 || min_s<0|| max_s<0)
					show_msg("The numbers for arrival and service time must be positive integers!","wrong time data");
				else if(min_a>max_a || min_s>max_s)
					show_msg("Minimum time should be less than maximum time!","minmax error");
				else 
				clk(k);
				
				}
				catch(Exception e1){
					e1.printStackTrace();
					show_msg("Give correct data (positive integer) for the simulation!","Wrong input");
				}
			}
		});
		btnStart_1.setBounds(182, 191, 89, 23);
		frame.getContentPane().add(btnStart_1);
		
		
		
		JLabel lblQueue = new JLabel("QUEUE 1");
		lblQueue.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblQueue.setBounds(10, 208, 77, 26);
		frame.getContentPane().add(lblQueue);
		
		JLabel lblQueue_1 = new JLabel("QUEUE 2");
		lblQueue_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblQueue_1.setBounds(441, 208, 89, 26);
		frame.getContentPane().add(lblQueue_1);
		
		taa3 = new JTextArea();
		taa3.setLineWrap(true);
		taa3.setBackground(Color.LIGHT_GRAY);
		taa3.setEditable(false);
		taa3.setText("");
		taa3.setBounds(874, 240, 400, 254);
		frame.getContentPane().add(taa3);
		
		JLabel lblQueue_2 = new JLabel("QUEUE 3");
		lblQueue_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblQueue_2.setBounds(874, 208, 77, 26);
		frame.getContentPane().add(lblQueue_2);
		
		JLabel lblNewLabel = new JLabel("Average waiting time :");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(10, 526, 146, 33);
		frame.getContentPane().add(lblNewLabel);
		
		info = new JTextArea();
		info.setEditable(false);
		info.setBounds(162, 532, 34, 23);
		frame.getContentPane().add(info);
		
		JLabel lblAverageServiceTime = new JLabel("Total clients served :");
		lblAverageServiceTime.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblAverageServiceTime.setBounds(10, 558, 146, 33);
		frame.getContentPane().add(lblAverageServiceTime);
		
		ser1 = new JTextArea();
		ser1.setEditable(false);
		ser1.setBounds(162, 566, 34, 23);
		frame.getContentPane().add(ser1);
		
		JLabel lblPeakHour = new JLabel("Peak hour :");
		lblPeakHour.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPeakHour.setBounds(10, 589, 89, 33);
		frame.getContentPane().add(lblPeakHour);
		
		p1 = new JTextArea();
		p1.setEditable(false);
		p1.setBounds(162, 600, 34, 23);
		frame.getContentPane().add(p1);
		
		JLabel label = new JLabel("Average waiting time :");
		label.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label.setBounds(450, 526, 146, 33);
		frame.getContentPane().add(label);
		
		info2 = new JTextArea();
		info2.setEditable(false);
		info2.setBounds(614, 530, 34, 23);
		frame.getContentPane().add(info2);
		
		JLabel lblTotalClientsServed = new JLabel("Total clients served");
		lblTotalClientsServed.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTotalClientsServed.setBounds(450, 558, 146, 33);
		frame.getContentPane().add(lblTotalClientsServed);
		
		ser2 = new JTextArea();
		ser2.setEditable(false);
		ser2.setBounds(614, 564, 34, 23);
		frame.getContentPane().add(ser2);
		
		JLabel label_2 = new JLabel("Peak hour :");
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_2.setBounds(450, 589, 146, 33);
		frame.getContentPane().add(label_2);
		
		p2 = new JTextArea();
		p2.setEditable(false);
		p2.setBounds(614, 595, 34, 23);
		frame.getContentPane().add(p2);
		
		JLabel label_3 = new JLabel("Average waiting time :");
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_3.setBounds(896, 526, 146, 33);
		frame.getContentPane().add(label_3);
		
		JLabel lblTotalClientsServed_1 = new JLabel("Total clients served");
		lblTotalClientsServed_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTotalClientsServed_1.setBounds(896, 556, 146, 33);
		frame.getContentPane().add(lblTotalClientsServed_1);
		
		JLabel label_5 = new JLabel("Peak hour :");
		label_5.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_5.setBounds(896, 589, 146, 33);
		frame.getContentPane().add(label_5);
		
		info3 = new JTextArea();
		info3.setEditable(false);
		info3.setBounds(1052, 532, 34, 23);
		frame.getContentPane().add(info3);
		
		ser3 = new JTextArea();
		ser3.setEditable(false);
		ser3.setBounds(1052, 564, 34, 23);
		frame.getContentPane().add(ser3);
		
		p3 = new JTextArea();
		p3.setEditable(false);
		p3.setBounds(1052, 595, 34, 23);
		frame.getContentPane().add(p3);
		
		JLabel lblMinutes = new JLabel("minutes");
		lblMinutes.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblMinutes.setBounds(658, 531, 65, 22);
		frame.getContentPane().add(lblMinutes);
		
		JLabel label_8 = new JLabel("minutes");
		label_8.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_8.setBounds(1096, 531, 65, 22);
		frame.getContentPane().add(label_8);
		
		JLabel label_7 = new JLabel("minutes");
		label_7.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_7.setBounds(206, 531, 65, 22);
		frame.getContentPane().add(label_7);
		
		JLabel lblOclock = new JLabel("o'clock");
		lblOclock.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblOclock.setBounds(206, 599, 65, 22);
		frame.getContentPane().add(lblOclock);
		
		JLabel label_10 = new JLabel("o'clock");
		label_10.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_10.setBounds(658, 600, 65, 22);
		frame.getContentPane().add(label_10);
		
		JLabel label_12 = new JLabel("o'clock");
		label_12.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_12.setBounds(1096, 594, 65, 22);
		frame.getContentPane().add(label_12);
		
		pr = new JTextArea();
		pr.setBounds(216, 636, 34, 20);
		frame.getContentPane().add(pr);
		
		JLabel lblNewLabel_1 = new JLabel("Total price paid for the queues :");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(10, 633, 214, 27);
		frame.getContentPane().add(lblNewLabel_1);
		
		tf = new JTextField();
		tf.setBounds(10, 11, 65, 19);
		frame.getContentPane().add(tf);
		tf.setColumns(10);
		
		JLabel lblGive = new JLabel("Simulation time in seconds (should be greater than 19)");
		lblGive.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblGive.setBounds(85, 8, 376, 23);
		frame.getContentPane().add(lblGive);
		
		m1 = new JTextField();
		m1.setBounds(10, 41, 65, 20);
		frame.getContentPane().add(m1);
		m1.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Minimum arrival time");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_2.setBounds(85, 42, 172, 14);
		frame.getContentPane().add(lblNewLabel_2);
		
		m2 = new JTextField();
		m2.setColumns(10);
		m2.setBounds(10, 72, 65, 20);
		frame.getContentPane().add(m2);
		
		m3 = new JTextField();
		m3.setColumns(10);
		m3.setBounds(10, 108, 65, 20);
		frame.getContentPane().add(m3);
		
		m4 = new JTextField();
		m4.setColumns(10);
		m4.setBounds(10, 139, 65, 20);
		frame.getContentPane().add(m4);
		
		JLabel lblMaximumArrivalTime = new JLabel("Maximum arrival time");
		lblMaximumArrivalTime.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblMaximumArrivalTime.setBounds(85, 75, 172, 14);
		frame.getContentPane().add(lblMaximumArrivalTime);
		
		JLabel lblMinimumServiceTime = new JLabel("Minimum service time");
		lblMinimumServiceTime.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblMinimumServiceTime.setBounds(85, 111, 172, 14);
		frame.getContentPane().add(lblMinimumServiceTime);
		
		JLabel lblMaximumServiceTime = new JLabel("Maximum service time");
		lblMaximumServiceTime.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblMaximumServiceTime.setBounds(85, 142, 172, 14);
		frame.getContentPane().add(lblMaximumServiceTime);
		
		JLabel lblPeakHourPer = new JLabel("Peak hour per shop");
		lblPeakHourPer.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPeakHourPer.setBounds(10, 671, 130, 27);
		frame.getContentPane().add(lblPeakHourPer);
		
		totp = new JTextArea();
		totp.setBounds(147, 674, 34, 20);
		frame.getContentPane().add(totp);
		
		JLabel label_1 = new JLabel("o'clock");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_1.setBounds(185, 673, 65, 22);
		frame.getContentPane().add(label_1);
	}
}
