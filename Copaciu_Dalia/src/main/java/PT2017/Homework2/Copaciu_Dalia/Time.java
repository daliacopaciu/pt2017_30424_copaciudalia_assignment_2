package PT2017.Homework2.Copaciu_Dalia;

import java.util.Random;
public class Time {
private int hour;
private int min;
public Time(){
	hour=0;
	min=0;
}
public void set_time(int h,int m){
	hour=h;
	min=m;
}
public int get_hour(){
	return hour;
}
public int get_min(){
	return min;
}
public void add_time(int m){    //adds a nr of minutes to the actual time; used for waiting time
	if(m>0){      //if the waiting time is incresed
		min+=m;
	}
	else{         //if the 1st client from te queue leaves, the waiting time is decreased
		int k=hour*60+min;  //transforming the time in minutes
		min=k-m;
	}
	while(min>59){
		hour+=1;
		min-=60;
	}
}
public void set_rand(int m,int max,char c){   //sets random hour and minute between m (minimum) and max
	if(c=='a'){   //for arrval time
	Random mn=new Random();
	min=mn.nextInt(60);
	Random h = new Random();
	hour=h.nextInt(max-m+1)+m;
	}
	else {    //for service time -> only minutes
		hour=0;
		Random mm=new Random();
		min=mm.nextInt(max-m+1)+m;
	}
	
}
public Time substr(Time a,Time b){   //substraction a-b in time format
	Time t=new Time();
	t.min=a.min-b.min;
	t.hour=a.hour-b.hour;
	if(t.hour<0) {
		t.hour=0;
		t.min=0;
		return t;
	}
    while(t.min<0 && hour>0){
        t.min+=60;
        t.hour--;
   } 
	return t;
}
public int calc_wait(Time a,Time b,int c){   //calculates the waiting time for a client =service time of the client before-(arrival time of the new client- arrival time of the client before)
	min=a.min-b.min;
	hour=a.hour-b.hour;
	if(hour<=0 && min<0) {
		return 0;
	}
    while(min<0 && hour>0){
        min+=60;
        hour--;
   } 
	Time t=new Time();
	t.set_time(hour, min);
	if(greater(tranform(c),t)){
		return substr(tranform(c),t).min;
	}
	return 0;
} 
public boolean greater(Time a, Time b){   //returns true is time a is greater than time b
	if(a.hour>b.hour)
		return true;
	if(a.hour==b.hour){
		if(a.min>b.min)
			return true;
		else return false;
	}
	return false;
	
}
public Time tranform(int m){   //transforms from minutes to hour:minutes =>ex: 79 minutes= 1h and 19 minutes
	Time t=new Time();
	while(m>59){
		t.hour+=1;
		m-=60;
	}
	t.min=m;
	return t;
}
public int mins(Time t){   //transforms from Time format to minutes
	if (t.hour==0)
			return t.min;
	else 
		while(t.hour>0){
			t.min+=60;
			t.hour--;
		}
	return t.min;
}
public String show(){   //displays the time in a nice format
	if(min<10)
	return hour+":0"+min;
	else return hour+":"+min;
}

}

