package PT2017.Homework2.Copaciu_Dalia;

import java.util.*;
public class Shop {
public static int MAX_CLIENTS=5;    //a new queue is added if all queues have more than 4 clients
public static int PRICE=35;        //each time a new queue is released, the cost increases with 35
public static int MAXQ=3;         //max nr of queues is 4
private int min_arrive;
private int max_arrive;
private int min_service;
private int max_service;
private int peak[];
private List<Queue> q;
private List<Client>cl;
private int cost;

public Shop(int mia,int mxa,int mis,int mxs){
	min_arrive=mia;
	max_arrive=mxa;
	min_service=mis;
	max_service=mxs;
	cost=0;
	q=new ArrayList<Queue>();
	cl=enter_clients(50);
	peak=new int[24];
	for(int i=0;i<24;i++){
		peak[i]=0;
	}
}
public List<Queue> queue(){
	return q;
}
public int peak(){
	int maxi=0, max=0;
	for(int i=0;i<24;i++){
		if(peak[i]>max){
			max=peak[i];
			maxi=i;
		}
	}
	return maxi;
}
public int get_cost(){
	return cost;
}
public Queue min_queue(){       //returns the queue with the min waiting time
	int min=100000;boolean ok=false;
	Queue qq=new Queue();
	if(!q.isEmpty()){
	  for(Queue q1:q)
		  if(q1.get_nrclient('p')<MAX_CLIENTS) 
		    if(q1.get_wait()<min){
		       min=q1.get_wait();
		       qq=q1;
		       ok=true;
		     } 
	  if(ok){
		  return qq;   //if a min queue was found return it
	  }
	  else if(q.size()<MAXQ ) {  //if all existing queues are full and there are queues available, open a new queue   
		  q.add(qq);  
		  cost+=PRICE;
		  return qq;}
	  
	}
	
	else{                             //if no queue exists at beginning, it is created an empty one
		q.add(qq);
		cost+=PRICE;
		return qq;
	}
	return qq;
}
public List<Client> enter_clients(int n){        //n=nr of clients arriving and entering queues
	List<Client>list=new ArrayList<Client>();
	for(int i=0;i<n;i++){
		Client c=new Client();
		c.get_time('a').set_rand(min_arrive, max_arrive,'a');
		c.get_time('s').set_rand(min_service, max_service, 's');
	    list.add(c);
	}
	Collections.sort(list, new Comparator<Client>(){    //placed in ascending order by time of arrivals
		public int compare(Client c1,Client c2){
			if(c1.get_time('a').get_hour()==c2.get_time('a').get_hour())
				return c1.get_time('a').get_min()-c2.get_time('a').get_min();
			else return c1.get_time('a').get_hour()-c2.get_time('a').get_hour();
		}
	});
   return list;
}
public void add_client(Queue qq){   //if qq is the minimum queue at the moment, it adds a client to is, otherwise it returns nothing
	List<Client>dlt=new ArrayList<Client>();
	Iterator<Client>it=cl.iterator();
	Client c=it.next();
	Queue min=min_queue();
	if(qq!=min) return;
    min.add_client(c);
    peak[c.get_time('a').get_hour()]++;
    Time aux=new Time();
    if(min.get_nrclient('p')==1)    {      //if is the 1st client
        c.set_Status(Status.being_served);
        min.served();
    }
	else c.set_Status(Status.waiting);
	c.wait(aux.calc_wait(c.get_time('a'), min.last(),min.get_wait()));  //sets waiting time
	dlt.add(c);
	cl.removeAll(dlt);
	
} 

public void client_come(int nn){   //adds a nr of clients to the queues for a nicer displaying of the queues
	for(int i=0;i<nn;i++){
	       Queue min=min_queue();
	       add_client(min);
	       }
}

public String[] update(Queue qq){  //remove a client then save the resulted queue in a string[]
	String[] rez=new String[100];          
		qq.remove_client();
		int i=0;
		for(Client c:qq.get_q()){
			  rez[i]=c.show();
			  i++;
			
		}
	return rez;
} 

}
