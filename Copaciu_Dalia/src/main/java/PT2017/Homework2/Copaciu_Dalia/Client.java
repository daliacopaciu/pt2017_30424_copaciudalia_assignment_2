package PT2017.Homework2.Copaciu_Dalia;
import java.util.UUID;
public class Client { 	
private String id;                  //unique id for a person
private Time arrive;
private Time service;
private Time wait;
private Status status;
public Client(){
	id=UUID.randomUUID().toString().substring(0,5);
	arrive=new Time();
	service=new Time();
	wait=new Time();
	status=Status.coming;
}
public String get_Status(){
	return status.name();
}
public void set_Status(Status s){
	status=s;
}
public Time get_time(char c){
	Time t;
	switch(c){
	case 'a':
		t= arrive;
		break;
	case 's':
		t= service;
		break;
	case 'w':
		t= wait;
		break;
	default:
		t=new Time();
		t.set_time(0, 0);
		break;
	}
	return t;
}
public void wait(int n){
	wait.set_time(0, 0);
	wait.add_time(n);
} 
public String show(){
	String rez="";
	rez="Client "+id+" arrives at "+arrive.show()+" service time: "+service.get_min()+" minutes, ";
	if(status==Status.being_served)
		rez+=" is being served.";
	else{
		rez+=Status.waiting.name();
	if(wait.get_hour()==0)
		rez+=" "+wait.get_min()+" minutes.";
    else rez+=" "+wait.show() + " hours.";
	}
	return rez;
}
}
